
import firebase from 'firebase'

// Router and State
// การ export แบบ default นิยมใช้ในกรณีที่ไฟล์มีแค่ฟังก์ชันเดียว
// และไม่มีการตั้งชื่อฟังก์ชั่น เพราะ มีอันเดียว
export default function ($locationProvider, $urlRouterProvider, $stateProvider, $compileProvider) {
  // แก้ปัญหา error หลังจาก minify ด้วย ng-annotate
  // error เกิดจาก UglifyJsPlugin เปลี่ยน parameter $locationProvider, $urlRouterProvider, $stateProvider ไปเป็นชื่ออื่น
  // ทำให้ angular มันไม่รู้จัก parameter ที่ชื่อเปลี่ยนไป
  // ดังนั้น ng-annotate จะมาทำหน้าที่ช่วยตรงนี้
  'ngInject'

  // Code initialize จากเว็บไซต์ firebase (เมนู Authentication >>> WEB SETUP)
  firebase.initializeApp({
    apiKey: 'AIzaSyC0oTKxbuh8n1-byM1sOP_y-Sp0h82AmMk',
    authDomain: 'first-firebase-4b860.firebaseapp.com',
    databaseURL: 'https://first-firebase-4b860.firebaseio.com',
    storageBucket: 'first-firebase-4b860.appspot.com',
    messagingSenderId: '676416331025'
  })

  // ทดสอบว่าเอา firebase เข้ามาใน app ได้
  // console.log(firebase)

  // Disable or remove ng-scope and ng-binding from generated HTML
  // Ref: http://stackoverflow.com/questions/27513515/angularjs-disable-or-remove-ng-scope-and-ng-binding-from-generated-html
  // Ref: http://stackoverflow.com/questions/28002113/how-compileprovider-debuginfoenabled-set-to-false-improve-performance-in-angula
  $compileProvider.debugInfoEnabled(false)

  // Router
  $locationProvider.html5Mode(true)
  $urlRouterProvider.otherwise('/')
  $stateProvider
    // Home
    .state('home', {
      abstract: true,
      template: require('./views/shared/layout.html'),
      controller: ('LayoutController'),
      controllerAs: 'vm'
    })
    .state('home.index', {
      url: '/',
      template: require('./views/home/home.html'),
      controller: ('HomeController'),
      controllerAs: 'vm'
    })
    // About
    .state('about', {
      abstract: true,
      template: require('./views/shared/layout.html'),
      controller: ('LayoutController'),
      controllerAs: 'vm'
    })
    .state('about.index', {
      url: '/about',
      template: require('./views/about/about.html'),
      controller: ('AboutController'),
      controllerAs: 'vm'
    })
    .state('about.detail', {
      url: '/about/detail',
      template: '<about-detail></about-detail>'
    })
    // Showcase
    .state('showcase', {
      abstract: true,
      template: require('./views/shared/layout.html'),
      controller: ('LayoutController'),
      controllerAs: 'vm'
    })
    .state('showcase.index', {
      url: '/showcase',
      template: require('./views/showcase/showcase.html'),
      controller: ('ShowcaseController'),
      controllerAs: 'vm'
    })
    .state('showcase.detail', {
      url: '/showcase/detail',
      template: '<showcase-detail></showcase-detail>'
    })
    // Web style guide
    .state('webStyleGuide', {
      abstract: true,
      template: require('./views/shared/layout.web-style-guide.html'),
      controller: ('LayoutWebStyleGuideController'),
      controllerAs: 'vm'
    })
    .state('webStyleGuide.index', {
      url: '/web-style-guide',
      template: require('./views/web-style-guide/web-style-guide.html'),
      controller: ('WebStyleGuideController'),
      controllerAs: 'vm'
    })

    // // Landing page
    // // state แม่
    // .state('auth', {
    //   abstract: true, // กำหนดให้เข้า auth จะเข้าได้เมื่อ state เป็นอันใดอันหนึ่งก่อน(signin/register)
    //   template: require('./views/auth.html'),
    //   // เอาออกเพราะ เราย้ายการควบคุมต่างๆ ไปที่ controller ย่อยอื่นๆ แล้ว
    //   // controller: ('AuthController'),
    //   // controllerAs: 'vm'

    //   // Redirect
    //   resolve: {
    //     redirectToHomeIfAuth
    //   }
    // })
    // // State ลูก ของ auth
    // // โดย state ลูกเหล่านี้จะมีชื่อ state แม่นำหน้าอยู่ >>> auth.xxx
    // // กดปุ่มที่ใช้ ui-sref แล้วจะเปลี่ยน template เฉพาะส่วน
    // .state('auth.signin', {
    //   url: '/',
    //   template: require('./views/signin.html'),
    //   controller: ('SignInController'),
    //   controllerAs: 'vm'
    // })
    // // State ลูก ของ auth
    // .state('auth.register', {
    //   url: '/register',
    //   template: require('./views/register.html'),
    // })
    // // การสร้าง layout template แม่ สามารถทำได้ 2 แบบ คือ
    // // แบบที่ 1 ประกาศ state แม่และให้ state ลูกมาใช้ template >>> auth, auth.signin, auth.register
    // // แบบที่ 2 ประกาศเป็น layout แม่ แล้วให้ state อื่นๆ มาใช้โดยระบุ parent
    // .state('layout', {
    //   template: require('./views/layout.html'),
    //   controller: ('LayoutController'),
    //   controllerAs: 'vm',
    //   // Redirect
    //   resolve: {
    //     redirectToAuthIfNotAuth
    //   }
    // })
    // .state('home', { // ไปหน้าใหม่ ดังนั้นไม่ต้องมี state แม่(auth.) นำหน้า
    //   url: '/home',
    //   parent: 'layout', // ระบุว่าใช้ template แม่ เป็นอะไร
    //   template: require('./views/home.html'),
    //   controller: ('HomeController'),
    //   controllerAs: 'vm'
    // })
}
