import angular from 'angular'
import Config from './app.config'
import Controllers from './controllers'
import Services from './services'
import Components from './components'

const app = angular.module('app', ['ui.router'])

// การตั้งตัวแปรคงที่เพื่อนำไปใช้แบบ global
// app.constant('LIB_URL', 'https://raw.githubusercontent.com/acoshift/course-ngfirebase/master/2-es6-fp/2-fp/library.json')
app.config(Config)

// Register controllers ทั้งหมดเข้าไปใน module
Controllers.forEach((controller) => {
  app.controller(controller.name, controller.controller)
})

// Register controllers ทั้งหมดเข้าไปใน module
Services.forEach((service) => {
  app.service(service.name, service.service)
})

// Register components ทั้งหมดเข้าไปใน module
Components.forEach((component) => {
  app.component(component.name, component)
})
