# Vendor
---

## vendor-extensions folder
มีหน้าที่เก็บไฟล์สไตล์ที่เขียนทับสไตล์ หรือเขียนสไตล์เพิ่มเติมจากที่เรานำของคนอื่นมาใช้ โดยไฟล์ในโฟลเดอร์นี้จะมีชื่อเดียวกับไฟล์สไตล์ที่อยู่ใน vendors folder
เพื่อให้สามารถระบุที่มาที่ไปของสไตล์ที่เขียนทับ หรือเขียนเพิ่มเติมได้ว่าเกี่ยวข้องกับไฟล์สไตล์ใดใน vendors folder
