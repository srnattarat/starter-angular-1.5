class AboutDetailController {
  constructor () {
    'ngInject'
  }

  $onInit () {
    console.log('AboutDetailController already!!!')
  }
  $postLink () {
    // jQuery
  }
}

export default {
  name: 'aboutDetail',
  template: require('../about/about-detail.component.html'),
  controller: AboutDetailController
  // ในกรณีที่เราไม่ต้องไว้จะต้องใช้ $ctrl ซึ่งเป็นค่า default ของ this ของ component แทน vm
  // controllerAs: 'vm'
}
