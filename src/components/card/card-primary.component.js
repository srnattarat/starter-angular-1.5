class CardPrimaryController {
  constructor () {
    'ngInject'
  }

  $onInit () {
    console.log('CardPrimaryController already!!!')
  }
  $postLink () {
    // jQuery
  }
}

export default {
  name: 'cardPrimary',
  template: require('../card/card-primary.component.html'),
  controller: CardPrimaryController,
  bindings: {
    // model จะรับตัวแปรเข้ามา
    model: '<',
    // onSubmit จะรับเป็นฟังก์ชั่นเข้ามา
    // onSubmit: '&',
    // mode รับเป็น string เข้ามา
    // โหมด เช่น add/edit มีข้อมูลไม่เหมือนกัน
    // ดังนั้นต้องแจ้งว่า component นี้เป็นโหมดอะไร เพื่อจะได้เลือกแสดงผล
    mode: '@'
  }
}
