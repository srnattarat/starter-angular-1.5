import AboutDetailComponent from './about/about-detail.component'
import ShowcaseDetailComponent from './showcase/showcase-detail.component'
import CardPrimaryController from './card/card-primary.component'

export default [
  AboutDetailComponent,
  ShowcaseDetailComponent,
  CardPrimaryController
]
