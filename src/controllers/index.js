// วิธีการเอาไฟล์ controller ต่างๆ มารวมไว้ในที่เดียวแล้ว ไปเรียกครั้งเดียวที่ app.module.js
// แล้วเวลา import ก็มาทำที่นี่ที่เดียว
import { LayoutController } from './shared/layout.controller'
import { LayoutWebStyleGuideController } from './shared/layout.web-style-guide.controller'
import { HomeController } from './home/home.controller'
import { AboutController } from './about/about.controller'
import { ShowcaseController } from './showcase/showcase.controller'
import { WebStyleGuideController } from './web-style-guide/web-style-guide.controller'

export default [
  { name: 'LayoutController', controller: LayoutController },
  { name: 'LayoutWebStyleGuideController', controller: LayoutWebStyleGuideController },
  { name: 'HomeController', controller: HomeController },
  { name: 'AboutController', controller: AboutController },
  { name: 'ShowcaseController', controller: ShowcaseController },
  { name: 'WebStyleGuideController', controller: WebStyleGuideController }
]
