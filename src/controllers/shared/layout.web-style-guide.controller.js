export class LayoutWebStyleGuideController {
  constructor () {
    'ngInject'
  }

  // Angular Life Cycle
  // https://toddmotto.com/angular-1-5-lifecycle-hooks
  // https://angular.io/docs/ts/latest/guide/lifecycle-hooks.html
  // http://blog.thoughtram.io/angularjs/2016/03/29/exploring-angular-1.5-lifecycle-hooks.html
  $onInit () {
    console.log('LayoutWebStyleGuideController already!!!')
  }

  // Post Link
  // จะ call ก็ต่อเมื่อ DOM ทุกอย่างถูกสร้างขึ้นมาแล้ว
  // เอาไว้ใช้กับ jQuery ที่ต้องอ้างอิง DOM ที่มี selector เช่น class/id
  $postLink () {
    // Sample jQuery
    // $('.card').matchheight()
  }
}
