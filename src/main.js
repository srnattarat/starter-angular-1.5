// Vendor modules
import angular from 'angular' // เท่ากับ const angular = require('angular')
import 'angular-ui-router'

// Environment check
if (is.chrome()) {
  console.log('Chrome')
} else if (is.firefox()) {
  console.log('Firefox')
}

// SASS Loader
require('./assets/sass/main.scss')

// Script loader
// เรียก jquery เข้ามาโดยใช้ script loader เพื่อใส่ script ไว้ที่ <head>
// กำหนด path file ที่อยู่ใน node_modules
// ไม่สามารถนำ script! ออกได้เพราะ เราต้องการให้ jquery (และอื่นๆ) run รูปแบบ script ที่เอาไปแปะไว้ที่ <head>
// ซึ่งถ้าเราเอาออก webppack มันก็จะงงว่าตกลงจะใช้ js loader ตัวไหน เพราะ default ที่เราตั้งไว้ที่ webpack คือ babel-loader ดังนั้นจึงจำเป็นต้องใส่ไว้
import 'script!jquery/dist/jquery.min.js'
import 'script!jquery-migrate/dist/jquery-migrate.min.js'
import 'script!is_js/is.min.js'

// Style loader
// import 'style!css!semantic-ui-css/semantic.min.css'
// เราทำการ config ที่ webpack แล้วว่าให้ใช้ style loader, css loader ไว้ จึงสามารถนำ style!css! ออกได้
// import 'semantic-ui-css/semantic.min.css'

// Project modules and controllers
// ต้อง import ทุก module, config, controller ที่สร้างขึ้นมา
// เพราะ เป็นตัว main ในการอ้างอิงไฟล์ต่างๆ
import './app.module'

angular.bootstrap(document, ['app'])
