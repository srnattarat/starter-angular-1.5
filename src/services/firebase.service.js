import firebase from 'firebase'
import { Observable, BehaviorSubject } from 'rxjs'

export class FirebaseService {
  constructor ($q, $rootScope) {
    'ngInject'

    // ประกาศเพื่อให้สามารถใช้ $rootScope จากที่อื่นได้
    this.$rootScope = $rootScope
    // ประกาศเพื่อให้สามารถใช้ $q เพื่อสร้าง promise
    this.$q = $q
  }
}
