// วิธีการเอาไฟล์ service ต่างๆ มารวมไว้ในที่เดียวแล้ว ไปเรียกครั้งเดียวที่ app.module.js
// แล้วเวลา import ก็มาทำที่นี่ที่เดียว
import { FirebaseService } from './firebase.service'
import { AnimationService } from './animation.service'

export default [
  { name: '$firebase', service: FirebaseService },
  { name: '$animation', service: AnimationService }
]
