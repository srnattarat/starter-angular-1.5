const webpack = require('webpack')
const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const WebpackNotifierPlugin = require('webpack-notifier')

// [1] สร้าง object และ export ออกไปเพื่อบอก webpack ว่า config เราเป็นยังไง
module.exports = {
  // [2] ระบุว่าโปรเจคเราอยูที่ folder ไหน
  context: path.join(__dirname, 'src'),
  devtool: 'source-map',
  debug: true,
  devServer: {
    historyApiFallback: true,
    inline: true
  },
  // [3] ระบุว่าโปรเจคเรา entry point อยูที่ไหน
  entry: {
    'main': './main.js'
  },
  // [4] ระบุว่าให้ทำการ output files ไปไว้ที่ folder ไหน
  // - เราไม่ต้องไปสร้าง folder ชื่อ build ไว้เพราะเวลา webpack ทำงานมันจะไปสร้างให้เอง
  // [7] หลังจากเรารันคำสั่ง webpack (ลงแบบ global ไว้แล้ว ดังนั้นไม่ต้องเข้าไปใน node_module/.bin/webpack แล้วค่อยรัน)
  // ก็จะมีการสร้าง folder "build" ขึ้นมาโดยข้างใน ประกอบด้วย index.html, main.xxxxx.js, main.xxxxx.js.map
  // ไฟล์ xxxxx.js.map เอาไว้ใช้ดูเวลาเกิด error แล้วสามารถรู้ได้ว่าเกิดที่บรรทัดไหน
  // ดังนั้นเวลาเอาขึ้น production เราไม่ต้องเอาไฟล์ xxxxx.js.map ขึ้นไปด้วย
  // ให้สังเกตว่าเวลาเรา build จะได้ไฟล์ที่มีชื่อยึกยือ ซึ่งส่วนนี้เรียกว่า hash เอาไว้ป้องกันการเก็บ cache ของ browser
  output: {
    path: path.join(__dirname, 'build'),
    filename: 'main.[hash].js',
    chunkFilename: '[hash].[id].chunk.js'
  },
  resolve: {
    modulesDirectories: ['web_modules', 'node_modules', 'bower_components']
  },
  module: {
    // preLoaders: [
    //   { test: /\.js$/, loader: 'source-map' },
    //   { test: /\.js$/, loader: 'standard', exclude: /(web_modules|node_modules|bower_components)/ }
    // ],
    // [5] loader เป็นการอ้างอิงว่าไฟล์ชนิดนี้จะให้ทำการโหลดด้วยอะไร
    // - เช่น .js ให้ใช้ babel โหลด (npm i --save babael-loader), .html ให้ใช้ html โหลด (npm i --save html-loader)
    // นอกจาก babel-loader แล้วเรายังต้องใช้ babel-preset อีก 2 ตัว คือ 2015, stage-0 (npm i --save babel-preset-es2015 babel-preset-stage-0)
    loaders: [
      // [6.3] เนื่องจากเรามีใช้ babel-loader, babel-preset-es2015, babel-preset-stage-0 ซึ่งเป็น plugin ด้วย ดังนั้น install ลงไปที่โปรเจคด้วย (plugin พวกนี้ต้องใช้ babel-core ด้วยอย่าลืมลง)
      // การใช้ ng-annotate เพื่อจัดการ error ที่เกิดจาก UglifyJsPlugin โดยการใส่ ng-annotate muj loader
      {
        test: /\.js$/,
        exclude: /(web_modules|node_modules|bower_components)/,
        loader: 'ng-annotate!babel?presets[]=es2015&presets[]=stage-0'
      },
      // [7] ลง script-loader, style-loader, css-loader, file-loader, url-loader
      // เพื่อสั่งให้ webpack เอาไปใส่ที่ <head>
      // file loader เราทำการ set ค่าไว้ว่าให้ save ไปที่ folder คือ assets/images/[hash].[ext]
      // [hash] เป็นชื่อที่จะเปลี่ยนทุกครั้งที่รูปที่เราเอามาใช้มีการเปลี่ยนแปลง ดังนั้น browser จะก็ไม่เก็บ cache ไว้ ทำให้รูปอัพเดตใหม่ทุกครั้ง
      {
        test: /\.(jpe?g|png|gif|svg|ico)$/i,
        loader: 'file?name=assets/images/[hash].[ext]'
      },
      {
        test: /\.(ttf|eot|woff|woff2)(\?[a-z0-9\.=]+)?$/,
        loader: 'file?name=assets/fonts/[hash].[ext]'
      },
      // [6.2] เนื่องจากเรามีใช้ html-loader ซึ่งเป็น plugin ด้วย ดังนั้น install ลงไปที่โปรเจคด้วย
      {
        test: /\.html?$/,
        loader: 'html',
        query: {
          interpolate: 'require'
        }
      },
      // html loader บรรทัดนี้เหมือนกับบรรทัดบน เพียงแต่ว่ามีการกำหนดค่าต่างๆ เพิ่มเช่น minimize
      // { test: /\.html?$/, loader: 'html?minimize=true&attrs[]=img:src&attrs[]=img:fallback-src' },
      // กรณีใช้ sass loader มันจะทำงานโดยมีขั้นตอนดังนี้
      // sass loader >>> ทำงานโดยแปลงเป็น css >>> แล้วเอา css ใส่มาให้ style loader >>> ทำงานแบบส่งต่อกันมา
      // ถ้าเอา 'style', 'css' loader ออก webpack มันก็จะงงว่า มรึงเอา sass มาทำอะไร
      {
        test: /\.scss$/,
        loaders: ['style', 'css', 'sass']
      },
      {
        test: /\.css$/,
        loaders: ['style', 'css']
      },
      // [7.1] อธิบาย url-loader >>> limit เป็นค่าที่ตั้งไว้ว่าถ้าไฟล์มีขนาดไม่เกิน 10000 ให้แปลงเป็น base64
      // และถ้าเกิดเกินขนาด 10000 มันก็จะไป save ไว้ที่ folder ที่เรากำหนดไว้
      {
        test: /\.woff$/,
        loader: 'url?limit=10000&mimetype=application/font-woff&name=assets/fonts/[hash].[ext]'
      },
      {
        test: /\.woff2$/,
        loader: 'url?limit=10000&mimetype=application/font-woff2&name=assets/fonts/[hash].[ext]'
      }
    ]
  },
  // [6] Plugin ที่ใช้ใน webpack จะต้อง install ลงไปที่โปรเจคด้วย
  plugins: [
    new WebpackNotifierPlugin({
      alwaysNotify: true
    }),
    // new webpack.optimize.DedupePlugin(),
    // new webpack.optimize.OccurenceOrderPlugin(),
    // การทำ minify html/css/js (ให้ทำตอน production เพราะ มันใช้เวลานานในการทำ)
    // ดังนั้น ตอนที่ dev อยู่ก็ให้ comment ไปก่อน
    // แต่ปัญหาจะมี error ว่า Error: [$injector:unpr] Unknown provider: e
    // เกิดขึ้นเนื่องจาก UglifyJsPlugin ไปทำการเปลี่ยนชื่อ parameter ของเราให้เป็นตัว e แทน
    // วิธีแก้ปัญหา คือ เราต้องบอก Angular ว่า parameter แต่ละตัวมัรชื่ออะไร
    // ซึ่งเราไม่ต้องไปประกาศตัวแปรเพื่อบอก เนื่องจากมีคนทำ script มาให้เราจัดการปัญหานี้แล้ว
    // ชื่อว่า ng-annotate (ไป install เข้ามา >>> npm i --save ng-annotate-loader)
    // https://github.com/olov/ng-annotate
    // new webpack.optimize.UglifyJsPlugin({ mangle: { keep_fnames: true }, sourcemap: false, compress: { warnings: false } }),
    // new webpack.optimize.CommonsChunkPlugin({ name: 'vendor', filename: 'vendor.[hash].js', minChunks: Infinity }),
    // [6.1] เป็น plugin ที่ใช้พื่อเอาไฟล์ที่เรา build แล้วไปใส่ไว้ใน index.html (npm i --save html-webpack-plugin)
    new HtmlWebpackPlugin({
      inject: true,
      template: './index.html'
    })
  ]
}
